[graphviz]
----
digraph G {
    rankdir = LR;
    BTS -> OsmoBSC [label="Abis/IP"];
    BTS -> OsmoPCU [label="GPRS/TRAU"];
    OsmoBSC -> OsmoMSC [label="3GPP AoIP"];
    OsmoBSC -> OsmoPCU [label="pcu_sock"];
    OsmoPCU -> OsmoSGSN [label="Gb/IP"];
    OsmoMSC -> OsmoHLR [label="GSUP"];
    OsmoSGSN -> OsmoHLR [label="GSUP"];
    OsmoSGSN -> OsmoGGSN [label="Gp"];
    OsmoGGSN -> Internet [label="Gi"];
    OsmoPCU [color=red];
    { rank=same OsmoBSC OsmoPCU }
}
----
