Cellular Network with Osmocom
=============================
:author:	Pau Espin Pedrol <pespin@espeweb.net>
:copyright:	2020 by Pau Espin Pedrol <pespin@espeweb.net> (License: CC-BY-SA)
:backend:	slidy
:max-width:	45em

== About me

* IT Engineer, Barcelona
* Free Software enthusiast
* Osmocom project contributor (https://osmocom.org/)
* Software developer at sysmocom - s.f.m.c. GmbH (https://www.sysmocom.de/)

== Cellular Network concepts

* Cell, Base Station (BS):

image::cell_tower.jpg[width="40%"]

* Mobile Station (MS), User Equipment (UE):

image::mobile_station.jpg[width="40%"]

== 3-sector poles

Usual cell configuration:

* 1 pole => 3 sectors
* 1 sector => 1 cell
* 1 cell => N Absolute Radio Frequency Channel Number (ARFCN) = N TRX
* 1 Base Transceiver Station (BTS) per sector or per pole

Radio frequencies:

* Several standardized GSM bands: GSM900, DCS1800, ...
* Example: GSM900 ARFCN 90: Downlink=953MHz Uplink=908 MHz

image::3sector_cell.png[width="50%"]

== Cellular Network generations

* Radio Access Network (RAN) vs Core Network
* Each generation reuses/extends existing nodes, or introduces new ones
* Naming rule of thumb: 3G (U-), 4G (E-), 5G (G-)
* Interaction between old and new nodes (eg. Handover, paging 2G <=> 3G, CSFB 2G <=> 4G)
* Each node-pair has a specified interface, usually with a different protocol stack
* Specifications: ETSI -> 3GPP TS AB.XYZ

image::multi_generation_diagram.jpg[]


== 2G Cellular Network architecture

* Circuit Switch (CS) vs Packet Switch (PS)

image::2g_network_diagram.jpg[width="50%"]

== 2G protocol stack

* Circuit Switch:

image::2g_proto_stack_cs.jpg[]

64kbps: E1/T1 lines?

* Packet Switch:

image::2g_proto_stack_ps.png[]


== Air interface: Um

* 1 cell => N TRX = N ARFCNs => FDMA
* 1 TRX => 8 TimeSlots => TDMA
* TimeSlots are configured with a given physical channel configuration (BCCH, SDCCH, TCH, PDCH)
* Logical channels allocated and multiplexed upon request on top of physical channels

image::2g_tdma_fdma.jpg[width="50%"]

* https://git.osmocom.org/osmo-bts/tree/src/common/scheduler_mframe.c
* https://git.osmocom.org/osmo-bsc/tree/doc/examples/osmo-bsc/osmo-bsc-minimal.cfg

== Operator interaction, roaming

* SS7 network, MAP protocol

image::inter_operator_conn.jpg[]

image::ss7_sigtran.png[]

== Osmocom

* Open Source Mobile Communications (https://osmocom.org/)
* Umbrella of open source software & hardware projects for mobile communication standards
* TETRA, E1/T1 hardware adapters, clock generators, analog protocols, ...
* 2G RAN & Core Network, Mobile Station
* 3G RAN & Core Network
* SIM card tools

== Osmocom 2G/3G RAN & Core network

* https://osmocom.org/projects/cellular-infrastructure/
* Free software, (A)GPL-licensed, public development
* All software can run on commodity hardware or/and VMs running GNU/Linux (NITB)
* User manuals for each software component available, tests available
* Nightly and latest packages available publicly for major distros as well as OpenEmbedded
* Interconnection with VoIP SIP possible through an external PBX
* Commercial support available
* Used by several commercial and non-commercial operators
* Interoperability tested against other commercial Core Network elements
* Tested in several big public events such as link:https://en.wikipedia.org/wiki/Chaos_Communication_Congress[CCC] or link:https://en.wikipedia.org/wiki/Chaos_Communication_Camp[CCCamp]

[graphviz]
----
digraph G {
  rankdir = LR;

  MS [label="MS\n(2G phone)"]
  UE [label="UE\n(3G phone)"]
  PBX [label="PBX\nAsterisk, FreeSwitch,\nKamailio, Yate, ..."]

  subgraph cluster_bts {
    BTS [rank="min"]
    PCU [rank="min"]
  }

  subgraph cluster_msc_mgw {
    label=MGCP;style=dotted
    MSC
    MGW1 [label="MGW"]
  }

  subgraph cluster_bsc_mgw {
    label=MGCP;style=dotted
    BSC
    MGW2 [label="MGW"]
  }

  hNodeB [shape="box",label="hNodeB\n(3G femto cell)"]

  MS -> BTS [label="Um"]
  MS -> PCU [style="dashed"]

  BTS -> BSC [label="Abis/IP"]
  STP [label="STP\n(SCCP/M3UA)"]
  BSC -> STP -> MSC [label="A"]
  MSC -> HLR [label="\nGSUP"]
  SGSN -> HLR [label="GSUP",style="dashed"]
  UE -> hNodeB [label="Uu"]
  UE -> hNodeB [style="dashed"]
  hNodeB -> HNBGW [label="Iuh"]
  HNBGW -> STP -> SGSN [label="IuPS",style="dashed"]
  HNBGW -> STP -> MSC [label="IuCS"]
  PCU -> SGSN [label="Gb",style="dashed"]
  SGSN -> GGSN [label="GTP-C",style="dashed"]
  SGSN -> GGSN [label="GTP-U(2G)",style="dashed"]
  hNodeB -> GGSN [label="GTP-U(3G)",style="dashed"]
  GGSN -> internet [label="tun",style="dashed"]

  BTS -> MGW2 -> MGW1 [label="RTP"]
  MGW1 -> MGW1 [label="RTP"]
  MGW2 -> MGW2 [label="RTP (LCLS)"]
  hNodeB -> MGW1 [label="IuUP/RTP"]

  MSC -> SIPConnector [label="MNCC socket"]

  SIPConnector -> PBX [label="SIP"]
  MGW1 -> PBX [label="RTP"]

  A, B, C, D [style="invisible"]
  A -> B [label="data (PS)",style="dashed"]
  C -> D [label="voice/SMS/USSD (CS)"]

}

----

== BTS Hardware

* link:https://osmocom.org/projects/osmobts[OsmoBTS]
** link:https://www.sysmocom.de/products/bts/[sysmoBTS]
** osmo-bts-trx + osmo-trx (ex OpenBTS, SDR):
*** UHD: link: https://osmocom.org/projects/osmotrx/wiki/Ettus_USRP_Family[Ettus B200], link:https://osmocom.org/projects/umtrx/wiki/UmTRX[Fairwaves UmTRX]
*** link:https://osmocom.org/projects/osmotrx/wiki/LimeSDR_Family[LimeSuite]: LimeSDR, LimeSDR-mini, LimeNET-micro
*** Other proprietary osmo-trx implementations
* ip.access link:https://osmocom.org/projects/openbsc/wiki/NanoBTS[nanoBTS]
* E1/T1 based BTSs decommissioned around the world:
** link:https://osmocom.org/projects/openbsc/wiki/Nokia_Site_family[Nokia InSite]
** link:https://osmocom.org/projects/openbsc/wiki/BS11[Siemens BS-11]
** link:https://osmocom.org/projects/openbsc/wiki/Ericsson_RBS[Ericsson RBS 2000]

* Virtual BTS (no HW required):
** osmo-bts-trx + fake_trx
** osmo-bts-virtual

BTS:
|=======
|sysmoBTS: | nanoBTS:
| image:sysmobts.png[width="50%"] | image:nanobts.jpg[width="50%"]
|Ericsson RBS200: | Siemens BS-11:
| image:ericsson_rbs2000.jpg[width="50%"] | image:bs11_bts.jpg[width="50%"]
|=======

SDR:
|=======
|Ettus B200: | LimeSDR:
| image:b200.jpg[width="50%"] | image:limesdr.jpg[width="50%"]
|=======

== Osmocom 2G Mobile Station

* Implementation of a full Mobile Station from Layer1 to Layer3
* Make phone calls, send SMS fully on Free Software
* Lower layers implemented in link:https://osmocom.org/projects/baseband/wiki[osmocom-bb] project
** TI Calypso baseband chip supported: Openmoko, Motorola C1<xy>, ...
* Upper layers implemented in link:https://osmocom.org/projects/baseband/wiki/Mobile[mobile] application
* Other upper layer tools available to use the baseband for any purpose (signal scanning, sniffing cell information, etc.)

|=======
|Openmoko:|Motorola C117:
| image:openmoko.jpg[] | image:c117_front.jpg[]
|=======

osmocom-bb applications:
|=======
| image:osmocombb-rssi-spectrum.jpg[] | image:osmocombb-rssi-sync-color.jpg[]
|=======

== 4G/5G Open source projects

* link:https://www.srslte.com/[srsLTE]: UE (SDR), eNodeB (SDR), minimal EPC
* link:https://open5gs.org/[Open5GS]: EPC

== Know more: Osmocom Users, deployments

* TIC (MX), Rhizomatica (US)
** https://www.rhizomatica.org/
** https://www.tic-ac.org/
** Talk: link:https://media.ccc.de/v/osmocon2018-67-community-cellular-network-implementations-in-latin-america[Community cellular network implementations in Latin America]
** Talk: link:https://media.ccc.de/v/osmocon2018-70-a-fault-tolerant-core-network-distributed-gsm[A Fault tolerant Core network - Distributed GSM]
** Talk: link:https://media.ccc.de/v/osmodevcon2019-117-doing-long-distance-digital-links-using-the-hf-radio-band-rhizomatica-s-hermes-project-in-mexico[Doing long distance digital links using the HF radio band]
* WaveMobile:
** http://wavemobile.com/
** Talk: link:https://media.ccc.de/v/osmocon2018-74-wavemobile-operating-a-small-real-world-roaming-2g-network-using-osmocom[WaveMobile: Operating a small, real-world roaming 2G network using Osmocom]
* OnWaves
** Talk: link:https://media.ccc.de/v/osmocon17-4011-showcase_running_a_commercial_cellular_network_with_osmobts_osmopcu_osmobsc_co[Running a commercial cellular network]
* Chaos Communication Congress:
** Talk: link:https://media.ccc.de/v/CBZ9Z3[Review of 34C3 cellular network]
** Talk: link:https://media.ccc.de/v/osmodevcon2019-105-35c3-aftermath[35C3 aftermath]

== Know more

* IRC: #osmocom @ freenode
* link:https://osmocom.org/projects/cellular-infrastructure/wiki/Mailing_Lists[Osmocom Mailing lists]
* link:https://osmocom.org/projects/cellular-infrastructure/wiki/Videos_of_Presentations[Osmocom Recorded Presentations, Talks]
* Contributing patches via link:https://osmocom.org/projects/cellular-infrastructure/wiki/Gerrit[Osmocom Gerrit]
